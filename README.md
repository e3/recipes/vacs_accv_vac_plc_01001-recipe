# vacs_accv_vac_plc_01001 conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/vacs_accv_vac_plc_01001"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS vacs_accv_vac_plc_01001 module
